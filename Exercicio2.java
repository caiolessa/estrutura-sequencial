/**
 * Exercicio 02
 * Faça um Programa que peça um numero e então mostre a mensagem O numero informado foi [nmero]..
 */
import java.util.Scanner;

public class Exercicio2 {
    public static void main(String[] args){
        
        Scanner entrada = new Scanner(System.in);
        
        System.out.printf("Digite um numero: ");
        int numero = entrada.nextInt();
        
        System.out.printf("O nmero informado foi: %d\n", numero);
        
    }
    
}
